// Проверка
n = 233434523424563451115;
m = 563224323563434641115;


function nm(n, m){
    return BigInt(n) * BigInt(m);
}

console.log(nm(n, m));

// Вариант 2

let num1 = String(n).split("").reverse().join("");
let num2 = String(m).split("").reverse().join("");


obj = {};

// Разбить по разрядам
for (var i_1 = 0; i_1 < num1.length; i_1++) {
    let symbol_1 = num1.charAt(i_1);
    for (var i_2 = 0; i_2 < num2.length; i_2++) {
        let symbol_2 = num2.charAt(i_2);
        let r = i_1 + i_2;
        if(r in obj){
            obj[r].push(Number(symbol_1) * Number(symbol_2));
        }else{
            obj[r] = [];
            obj[r].push(Number(symbol_1) * Number(symbol_2));
        }

    }
}
// Сумма значений в каждом массиве
for (key in obj) {
    obj[key] = obj[key].reduce((a, b) => a + b)
}

// console.log(obj);

// Перекидывание десятков
let res = {};
let previousKey;
let previousValue;
for (key2 in obj) {
    if(key2 !== '0'){
        previousKey = String(Number(key2) - 1);
        if(obj[previousKey] > 9){
            previousValue = Math.floor(obj[previousKey] / 10);
            obj[key2] = obj[key2] + previousValue
        }
    }

    res[key2] = obj[key2] % 10

}

if(obj[key2] > 0){
    res[Object.keys(res).length] = Math.floor(obj[key2] / 10);
}

// Конкатенация
let res_string = '';
for (key3 in res) {
    res_string = res_string + res[key3];
}

let answer = res_string.split("").reverse().join("");

console.log(answer);





























