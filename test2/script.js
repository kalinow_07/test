

function nok(x, y){
    let min = Math.min(x, y);
    let max = Math.max(x, y);
    while ((max % min) !== 0) {
        max = max + Math.max(x, y);
    }
    return max
}

function nod(x, y){
    let n = 1;
    let min = Math.min(x, y);
    let answer = 1;
    while (n <= min) {
       if(n > 100) return true;
        n++;
        if((x % n) === 0 && (y % n) === 0 ){
            answer = n;
        }
    }
    return answer;
}


function my(a, b, c, d){
    let result_nok = nok(b, d);

    let aa = a * (result_nok / b)
    let cc = c * (result_nok / d)

    let sum = aa + cc

    let result_nod = nod(sum, result_nok)
    return {
        n: sum / result_nod,
        m: result_nok / result_nod,
    }
}


console.log(my(29,30, 44, 45));